﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.SceneManagement;

public class FinalSelectScript : MonoBehaviour {

    public string[] sceneFileNames = { "blankData.txt", "planetData.txt", "cloudsData.txt", "basementData.txt", "mountainsData.txt" };

    public string[] audioFileNames = { "dangerData.txt", "nightData.txt", "battleData.txt", "universeData.txt", "tavernData.txt" };

    FileInfo theSourceFile = null;
    StreamReader reader = null;

    int finalScene = 6;
    int finalAudio = 6;

	// Use this for initialization
	void Start () {

        int maxidx = -1;

        float[] avs = { 0, 0, 0, 0, 0 };
        for (int i = 0; i < 5; i+=1) {

            theSourceFile = new FileInfo(sceneFileNames[i]);
            reader = theSourceFile.OpenText();

            float total = 0;
            float num = -1;
            string text = " ";
            while (text != null) {
                text = reader.ReadLine();
                if (text != null) {
                    float val = float.Parse(text);
                    if (val > 1) {
                        total += val;
                        num += 1;
                    }
                }
            }
            avs[i] = total / num;

            if (maxidx == -1 || avs[maxidx] > avs[i]) {
                maxidx = i;
            }
        }

        finalAudio = maxidx;


        maxidx = -1;

        float[] navs = { 0, 0, 0, 0, 0 };
        for (int i = 0; i < 5; i += 1)
        {

            theSourceFile = new FileInfo(audioFileNames[i]);
            reader = theSourceFile.OpenText();

            float total = 0;
            float num = -1;
            string text = " ";
            while (text != null)
            {
                text = reader.ReadLine();
                if (text != null)
                {
                    float val = float.Parse(text);
                    if (val > 1)
                    {
                        total += val;
                        num += 1;
                    }
                }
            }
            navs[i] = total / num;

            if (maxidx == -1 || navs[maxidx] > navs[i])
            {
                maxidx = i;
            }
        }

        finalScene = maxidx;

	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log(finalScene);
        if (Input.GetKeyDown("space"))
        {
            StreamWriter sw = File.CreateText("soundSelect.txt");
            sw.WriteLine(finalAudio);
            sw.Flush();
            SceneManager.LoadScene(finalScene);
        }	
	}
}
