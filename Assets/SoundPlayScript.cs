﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEditor;

public class SoundPlayScript : MonoBehaviour {

    public AudioSource audioData1;
    public AudioSource audioData2;
    public AudioSource audioData3;
    public AudioSource audioData4;
    public AudioSource audioData5;

    public int defaultSound = 6;

    string fileName = "soundSelect.txt";

	// Use this for initialization
	void Start () {
        AudioSource[] audioData = { audioData1, audioData2, audioData3, audioData4, audioData5 };
        if (defaultSound < 6 && defaultSound >= 0) {
            audioData[defaultSound].Play(0);
        } else if (File.Exists(fileName)) {

            FileInfo theSourceFile = new FileInfo(fileName);
            StreamReader reader = theSourceFile.OpenText();

            string text = reader.ReadLine();
            if (text != null) {
                audioData[int.Parse(text)].Play(0);
            }

            File.Delete(fileName);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
