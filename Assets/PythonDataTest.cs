﻿using UnityEngine;
using System.Collections;
using System.IO;



public class PythonDataTest : MonoBehaviour
{
	string m_Path;

	public string fileName = "blankData.txt";

	System.Diagnostics.Process p;

	int i;

	FileInfo theSourceFile = null;
	StreamReader reader = null;
	string text = " "; // assigned to allow first line to be read below

    StreamWriter sw;

	void Start ()
	{
		Screen.fullScreen = !Screen.fullScreen;
		i = 0;
		m_Path = Application.dataPath;

		theSourceFile = new FileInfo ("Assets/data.txt");
		reader = theSourceFile.OpenText();

        sw = File.CreateText(fileName);

		var runProcess = false;

        if (runProcess)
        {
            Debug.Log("Start read");

            p = new System.Diagnostics.Process();
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            //p.StartInfo.CreateNoWindow = true;
            //p.EnableRaisingEvents = true;
            //p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            p.StartInfo.FileName = m_Path + "/webcam-pulse-detector-no_openmdao/get_pulse.py";
            //p.StartInfo.FileName = m_Path + "/pythontest.py";
            //p.StartInfo.Arguments = "optional arguments separated with spaces";

            p.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(
                (s, e) =>
                {
                    Debug.Log("DATA: " + i + " - " + e.Data);
                    Debug.Log(s);
                }
            );
            p.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(
                (s, e) =>
                {
                    Debug.Log("ERROR:" + i + " - " + e.Data);
                }
            );

            p.Start();
            p.BeginOutputReadLine();

            Debug.Log("Read end");
        }
	}

	// Update is called once per frame
	void Update () {

		i = i + 1;

		text = reader.ReadLine();
		if (text != null) {
            Debug.Log(text);
            sw.WriteLine (text);
            sw.Flush();
		}
	}
}