﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RateItem : MonoBehaviour {

    public GameObject userImage;
    public GameObject playButton;
    public Image[] rateImages;
    public Sprite[] starImages;

    [HideInInspector]
    public int rate = 0, index;
    [HideInInspector]
    public bool isImage;

	// Use this for initialization
	void Start () {
        playButton.GetComponent<Button>().onClick.AddListener(OnPlayAudio);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetValue(bool bImage, int _index)
    {
        rate = 0;
        ShowRate(0);
        isImage = bImage;
        index = _index;
        userImage.SetActive(bImage);
        playButton.SetActive(!bImage);
        if (isImage)
            userImage.GetComponent<Image>().sprite = WordManager.instance.images[index];
    }

    public void ShowRate(int r)
    {
        for (int i = 0; i < rateImages.Length; i++)
        {
            if(i < r)
            {
                rateImages[i].sprite = starImages[1];
                rateImages[i].color = new Color(1, 1, 1, 1);
            }
            else
            {
                rateImages[i].sprite = starImages[0];
                rateImages[i].color = new Color(1, 1, 1, 0.5f);
            }
        }
    }

    public void OnPlayAudio()
    {
        WordManager.instance.audioSource.Stop();
        WordManager.instance.audioSource.PlayOneShot(WordManager.instance.audios[index]);
    }

    public void OnRateButtonEnter(int k)
    {
        ShowRate(k);
    }

    public void OnRateButtonExit(int k)
    {
        ShowRate(rate);
    }

    public void OnRateButtonClick(int k)
	{
        rate = k;
        ShowRate(rate);
    }
}
