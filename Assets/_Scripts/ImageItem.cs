﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageItem : MonoBehaviour {

    public Image image;
    public Text valueText;
    public Text rateText;

    public void SetValue(int index, double value)
    {
        image.sprite = WordManager.instance.images[index];
        valueText.text = value.ToString();
        if (WordManager.instance.imageRateItems[index].rate == 0)
            rateText.text = "";
        else
            rateText.text = WordManager.instance.imageRateItems[index].rate.ToString();
    }
}
