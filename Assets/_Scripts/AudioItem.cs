﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AudioItem : MonoBehaviour {

    public Button playButton;
    public Text valueText;
    public Text rateText;

    [HideInInspector]
    public int audioIndex;

	private void Awake()
	{
        playButton.onClick.AddListener(OnPlay);
	}

	public void SetValue(int index, double value)
    {
        audioIndex = index;
        valueText.text = value.ToString();
        if (WordManager.instance.audioRateItems[index].rate == 0)
            rateText.text = "";
        else
            rateText.text = WordManager.instance.audioRateItems[index].rate.ToString();
    }

	public void OnPlay()
	{
        WordManager.instance.audioSource.Stop();
        WordManager.instance.audioSource.PlayOneShot(WordManager.instance.audios[audioIndex]);
	}
}
