﻿using UnityEngine;
using System.Collections;
using System.IO;
using UnityEngine.UI;
using System.Collections.Generic;

public class WordManager : MonoBehaviour
{
    public static WordManager instance;

    [Header("Panel")]
    public GameObject splashPanel;
    public GameObject mainPanel;

    [Header("Detail")]
    public Button startButton;
    public GameObject imagePanel;
    public Image userImage;
    public GameObject audioPanel;
    public AudioSource audioSource;
    public GameObject reportPanel;
    public RateItem[] imageRateItems;
    public RateItem[] audioRateItems;
    public GameObject displayPanel;
    public ImageItem[] imageItems;
    public AudioItem[] audioItems;

    public Sprite[] images;
    public AudioClip[] audios;

    private int imageCount = 11, audioCount = 12;
    private int[] imageSequence = new int[11];
    private int[] audioSequence = new int[12];
    private double[] avgImageValues = new double[11];
    private double[] avgAudioValues = new double[12];
    private List<double> bpmList = new List<double>();

    private bool isStarted = false;
    private int curIndex = 0;

    [Header("File Info")]
    string m_Path;
    //public string fileName = "blankData.txt";
    System.Diagnostics.Process p;
    int i;
    FileInfo theSourceFile = null;
    StreamReader reader = null;
    string text = " "; // assigned to allow first line to be read below

    //StreamWriter sw;

    void Start()
    {
        instance = this;
        Application.runInBackground = true;

        splashPanel.GetComponent<Button>().onClick.AddListener(OnSplashEnter);
        startButton.onClick.AddListener(OnStart);

        Screen.fullScreen = !Screen.fullScreen;
        i = 0;
        m_Path = Application.dataPath;

        theSourceFile = new FileInfo("Assets/data.txt");
        reader = theSourceFile.OpenText();

        //sw = File.CreateText(fileName);

        var runProcess = false;

        if (runProcess)
        {
            Debug.Log("Start read");

            p = new System.Diagnostics.Process();
            p.StartInfo.RedirectStandardError = true;
            p.StartInfo.RedirectStandardOutput = true;
            p.StartInfo.UseShellExecute = false;
            //p.StartInfo.CreateNoWindow = true;
            //p.EnableRaisingEvents = true;
            //p.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            p.StartInfo.FileName = m_Path + "/webcam-pulse-detector-no_openmdao/get_pulse.py";
            //p.StartInfo.FileName = m_Path + "/pythontest.py";
            //p.StartInfo.Arguments = "optional arguments separated with spaces";

            p.OutputDataReceived += new System.Diagnostics.DataReceivedEventHandler(
                (s, e) =>
                {
                    Debug.Log("DATA: " + i + " - " + e.Data);
                    Debug.Log(s);
                }
            );
            p.ErrorDataReceived += new System.Diagnostics.DataReceivedEventHandler(
                (s, e) =>
                {
                    Debug.Log("ERROR:" + i + " - " + e.Data);
                }
            );

            p.Start();
            p.BeginOutputReadLine();

            Debug.Log("Read end");
        }
    }

    // Update is called once per frame
    void Update()
    {
        i = i + 1;
        text = reader.ReadLine();
        if (text != null)
        {
            if(isStarted)
            {
                double v = 0;
                double.TryParse(text, out v);
                bpmList.Add(v);
            }
            //Debug.Log(text);
            //sw.WriteLine(text);
            //sw.Flush();
        }
    }

    public void OnSplashEnter()
    {
        splashPanel.SetActive(false);
        mainPanel.SetActive(true);
    }

    public void OnStart()
    {
        StartCoroutine(StartDisplay());
    }

    List<int> randArray = new List<int>();

    public void InitImage()
    {
        // Randomize Font Sequence
        RandomArray(imageCount);
        for (int j = 0; j < imageCount; j++)
            imageSequence[j] = randArray[j];
    }

    public void InitAudio()
    {
        // Randomize Color Sequence
        RandomArray(audioCount);
        for (int j = 0; j < audioCount; j++)
            audioSequence[j] = randArray[j];
    }

    public void RandomArray(int size)
    {
        randArray.Clear();
        for (int i = 0; i < size; i++)
            randArray.Add(i);
        for (int i = 0; i < size; i++)
        {
            int rd = Random.Range(i, size);
            int tmp = randArray[i];
            randArray[i] = randArray[rd];
            randArray[rd] = tmp;
        }
    }

    IEnumerator StartDisplay()
    {
        startButton.gameObject.SetActive(false);
        imagePanel.gameObject.SetActive(false);
        audioPanel.gameObject.SetActive(false);
        displayPanel.gameObject.SetActive(false);
        yield return new WaitForSeconds(1);
        isStarted = true;

        for (int k = 0; k < 3; k++)
        {
            InitImage();
            for (curIndex = 0; curIndex < imageCount; curIndex++)
            {
                bpmList.Clear();

                imagePanel.SetActive(true);
                userImage.GetComponent<Image>().sprite = images[imageSequence[curIndex]];

                yield return new WaitForSeconds(5);

                // Calculate the values
                double sum = 0;
                for (int i = 0; i < bpmList.Count; i++)
                    sum += bpmList[i];
                avgImageValues[imageSequence[curIndex]] = sum / bpmList.Count;
                imagePanel.gameObject.SetActive(false);

                yield return new WaitForSeconds(2);
            }
        }

        audioPanel.SetActive(true);

        for (int k = 0; k < 3; k++)
        {
            InitAudio();
            for (curIndex = 0; curIndex < audioCount; curIndex++)
            {
                bpmList.Clear();

                audioSource.Stop();
                audioSource.PlayOneShot(audios[audioSequence[curIndex]]);

                yield return new WaitForSeconds(audios[audioSequence[curIndex]].length);

                // Calculate the values
                double sum = 0;
                for (int i = 0; i < bpmList.Count; i++)
                    sum += bpmList[i];
                avgAudioValues[audioSequence[curIndex]] = sum / bpmList.Count;

                yield return new WaitForSeconds(2);
            }
        }

        isStarted = false;

        audioPanel.SetActive(false);

        OnReportPanel();
    }

    public void OnReportPanel()
    {
        reportPanel.SetActive(true);
        for (int i = 0; i < imageCount; i++)
        {
            imageRateItems[i].SetValue(true, i);
        }
        for (int i = 0; i < audioCount; i++)
        {
            audioRateItems[i].SetValue(false, i);
        }
    }

    public void OnReportNextButton()
    {
        reportPanel.SetActive(false);
        DisplayValue();
    }

    List<int> indexList = new List<int>();

    public void DisplayValue()
    {
        startButton.gameObject.SetActive(true);
        displayPanel.gameObject.SetActive(true);

        // Image
        indexList.Clear();
        for (int i = 0; i < imageCount; i++)
            indexList.Add(i);
        for (int i = 0; i < imageCount; i++)
        {
            for (int j = i + 1; j < imageCount; j++)
            {
                if (avgImageValues[indexList[i]] > avgImageValues[indexList[j]])
                {
                    int tmp = indexList[i];
                    indexList[i] = indexList[j];
                    indexList[j] = tmp;
                }
            }
        }
        for (int i = 0; i < imageCount; i++)
        {
            imageItems[i].SetValue(indexList[i], avgImageValues[indexList[i]]);
        }

        // Audio
        indexList.Clear();
        for (int i = 0; i < audioCount; i++)
            indexList.Add(i);
        for (int i = 0; i < audioCount; i++)
        {
            for (int j = i + 1; j < audioCount; j++)
            {
                if (avgAudioValues[indexList[i]] > avgAudioValues[indexList[j]])
                {
                    int tmp = indexList[i];
                    indexList[i] = indexList[j];
                    indexList[j] = tmp;
                }
            }
        }
        for (int i = 0; i < audioCount; i++)
        {
            audioItems[i].SetValue(indexList[i], avgAudioValues[indexList[i]]);
        }
    }
}